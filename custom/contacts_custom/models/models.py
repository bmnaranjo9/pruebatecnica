# -*- coding: utf-8 -*-

from odoo import models, fields, api
import string


class ResCountryCity(models.Model):
    _name = 'res.country.city'
    _description = 'Ciudad'
    _rec_name = 'name'
    _order = 'name asc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'El nombre de la ciudad debe ser único'), ]

    """MANY2ONE"""
    state_id = fields.Many2one('res.country.state', string='Departamento', required=True)
    """CHAR"""
    name = fields.Char(string='Ciudad', required=True)

    @api.model_create_multi
    def create(self, vals_list):
        res = super(ResCountryCity, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res
