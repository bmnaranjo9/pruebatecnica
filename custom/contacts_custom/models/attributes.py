from odoo import models, fields, api


class ContactsCustom(models.Model):
    _inherit = 'res.partner'

    """MANY2ONE"""
    city_id = fields.Many2one('res.country.city', string='Ciudad')
    state_id = fields.Many2one('res.country.state', string='Estado')

    @api.onchange('city_id')
    def _onchange_city_id(self):
        self.state_id = self.city_id.state_id.id

    @api.onchange('state_id')
    def _onchange_state_id(self):
        """VERIFICA QUE EL ESTADO COINCIDA CON LA CIUDAD"""
        if self.city_id.state_id.id != self.state_id.id:
            self.city_id = False

    @api.onchange('company_type')
    def _onchange_company_type(self):
        if self.company_type == 'company':
            self.parent_id = False
