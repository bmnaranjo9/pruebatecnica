# -*- coding: utf-8 -*-

from odoo import models, fields, api
import string
from odoo.exceptions import ValidationError


class RhRh(models.Model):
    _name = 'rh.rh'
    _description = 'Grupo Sanguineo'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre del grupo sanguineo debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'rh_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(RhRh, self).create(vals_list)
        for rec in res:
            """CONVIERTE LA PRIMERA LETRA EN MAYUSCULA"""
            rec.name = string.capwords(rec.name)
        return res


class ResBank(models.Model):
    _inherit = 'res.bank'

    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'bank_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)

    @api.model_create_multi
    def create(self, vals_list):
        res = super(ResBank, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res


class HrCompensationBox(models.Model):
    _name = 'hr.compensation.box'
    _description = 'Caja de compensacion'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre de la caja de compensacion debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'compensation_box_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrCompensationBox, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res


class HrEps(models.Model):
    _name = 'hr.eps'
    _description = 'Eps'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre del eps debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'eps_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrEps, self).create(vals_list)
        for rec in res:
            """CONVIERTE LA PRIMERA LETRA EN MAYUSCULA"""
            rec.name = string.capwords(rec.name)
        return res


class HrArl(models.Model):
    _name = 'hr.arl'
    _description = 'Arl'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre del arl debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'arl_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrArl, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res


class HrLayoffs(models.Model):
    _name = 'hr.layoffs'
    _description = 'Cesantia'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre de la cesantia debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'layoffs_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrLayoffs, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res


class HrPension(models.Model):
    _name = 'hr.pension'
    _description = 'Pension'
    _order = 'name'
    _rec_name = 'name'

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "El nombre de la pension debe ser unico"),
    ]

    """CHAR"""
    name = fields.Char(string='Nombre', required=True, copy=False)
    """BOOLEAN"""
    active = fields.Boolean(string='Activo', default=True)
    """ONE2MANY"""
    employee_ids = fields.One2many('hr.employee', 'pension_id', string='Empleados')
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia', )

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrPension, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res


class InheritedContacts(models.Model):
    _inherit = 'res.partner'

    def write(self, vals):
        res = super(InheritedContacts, self).write(vals)
        for rec in self:
            """SI ACTUALIZA EL NOMBRE DEL CONTACTO, ACTUALIZA EL NOMBRE DEL EMPLEADO"""
            if 'name' in vals:
                self.env['hr.employee'].search([('user_partner_id', '=', rec.id)]).name = vals['name']
        return res


class HrCustom(models.Model):
    _inherit = 'hr.employee'

    """NO SE PUEDE REPETIR EL NOMBRE DEL EMPLEADO, NI EL CAMPO (ADDRESS_HOME_ID)"""
    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         "El nombre del empleado debe ser unico, por favor verifique e intente nuevamente."),
        ('address_home_id_uniq', 'unique (address_home_id)',
         "El contacto ya tiene un empleado asociado, por favor verifique e intente nuevamente."),
    ]

    """ONE2MANY"""
    example_son_ids = fields.One2many('example.son', 'employee_id', string='Hijos')
    """MANY2ONE"""
    identification_type_id = fields.Many2one('l10n_latam.identification.type', string='Tipo de identificacion',
                                             tracking=True, domain=[('id', 'in', [5, 9])])
    rh_id = fields.Many2one('rh.rh', string='Grupo sanguineo', tracking=True)
    city_of_birth = fields.Many2one('res.country.city', string='Ciudad de nacimiento', tracking=True)
    state_of_birth = fields.Many2one('res.country.state', string='Departamento de nacimiento', tracking=True,
                                     compute='_compute_state_of_birth', compute_sudo=True, readonly=False, store=True,
                                     inverse='_inverse_state_of_birth')
    bank_id = fields.Many2one('res.bank', string='Banco', )
    city_home = fields.Many2one('res.country.city', string='Ciudad de residencia', tracking=True)
    state_home = fields.Many2one('res.country.state', string='Departamento de residencia', tracking=True,
                                 compute='_compute_state_home', compute_sudo=True, readonly=False, store=True)
    compensation_box_id = fields.Many2one('hr.compensation.box', string='Caja de compensacion', tracking=True)
    eps_id = fields.Many2one('hr.eps', string='Eps', tracking=True)
    arl_id = fields.Many2one('hr.arl', string='Arl', tracking=True)
    layoffs_id = fields.Many2one('hr.layoffs', string='Cesantias', tracking=True)
    pension_id = fields.Many2one('hr.pension', string='Pension', tracking=True)
    """CHAR"""
    identification_id = fields.Char(string='Nro. de identificacion', groups=None, )
    number_account = fields.Char(string='Numero de cuenta', )
    private_email = fields.Char(string='Correo electronico', groups=None, tracking=True, readonly=False)
    age = fields.Char(string='Edad', tracking=True, compute='_compute_age', compute_sudo=True, readonly=False,
                      store=True, inverse='_inverse_age')
    phone = fields.Char(related='address_home_id.phone', related_sudo=False, readonly=False, string="Celular",
                        groups=None,
                        tracking=True)
    address_home = fields.Char(string='Direccion')
    relationship_contact = fields.Char(string='Relación o parentesco')
    """SELECTION"""
    certificate_type = fields.Selection(
        [('1', 'Bachiller'), ('2', 'Tecnico'), ('3', 'Tecnologo'), ('4', 'Pregrado'), ('5', 'Postgrado'),
         ('6', 'Maestria')], string='Nivel educativo')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], groups=None, tracking=True)
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Estado civil', groups=None, tracking=True)
    """INTEGER"""
    related_example_son = fields.Integer(string='Hijos', compute='_compute_related_example_son', store=True)

    """METHODS (CREATE, WRITE)"""

    def write(self, vals):
        res = super(HrCustom, self).write(vals)
        for rec in self:
            """VALIDA QUE EL NUMERO DE LA CUENTA BANCARIA SEAN SOLO NUMEROS"""
            if 'number_account' in vals:
                if not vals['number_account'].isdigit():
                    raise ValidationError(
                        'El numero de la cuenta bancaria debe ser de solo numeros, por favor verifique e intente nuevamente.')
            """VALIDA QUE EL NUMERO DE IDENTIFICACION SEA SOLO NUMEROS"""
            if 'identification_id' in vals:
                if not vals['identification_id'].isdigit():
                    raise ValidationError(
                        'El numero de identificacion debe ser de solo numeros, por favor verifique e intente nuevamente.')
        return res

    """COMPUTED FIELDS"""

    @api.depends('example_son_ids')
    def _compute_related_example_son(self):
        for record in self:
            record.related_example_son = len(record.example_son_ids)

    @api.depends('city_of_birth')
    def _compute_state_of_birth(self):
        if self.city_of_birth:
            self.state_of_birth = self.city_of_birth.state_id.id
        else:
            self.country_id = False

    @api.onchange('state_of_birth')
    def _inverse_state_of_birth(self):
        if self.state_of_birth:
            """VERIFICA QUE ESE ESTADO TENGA ASOCIADO ESA CIUDAD"""
            if self.city_of_birth.state_id.id != self.state_of_birth.id:
                self.city_of_birth = False
        else:
            self.country_id = False

    @api.depends('birthday')
    def _compute_age(self):
        today = fields.Date.today()
        for year in self:
            if year.birthday:
                age = int((today - year.birthday).days / 365.25)
                year.age = str(age) + ' años'
            else:
                year.age = False

    @api.depends('age')
    def _inverse_age(self):
        today = fields.Date.today()
        for year in self:
            if year.birthday:
                age = int((today - year.birthday).days / 365.25)
                year.age = str(age) + ' años'
            else:
                year.age = False

    @api.depends('city_home')
    def _compute_state_home(self):
        if self.city_home:
            self.state_home = self.city_home.state_id.id
        else:
            self.state_home = False

    """FUNCTIONS"""

    @api.onchange('address_home_id')
    def _onchange_address_home_id(self):
        if self.address_home_id:
            self.name = self.address_home_id.name
            self.private_email = self.address_home_id.email
            self.phone = self.address_home_id.phone
            self.address_home = self.address_home_id.street
            self.identification_type_id = self.address_home_id.l10n_latam_identification_type_id
            self.identification_id = self.address_home_id.vat
            self.city_home = self.address_home_id.city_id
            self.state_home = self.address_home_id.state_id


    def action_view_example_son(self):
        return {
            'name': 'Hijos',
            'view_mode': 'tree,form',
            'res_model': 'example.son',
            'domain': [('employee_id', '=', self.id)],
            'type': 'ir.actions.act_window',
            'context': {'default_employee_id': self.id},
        }
