from odoo import models, fields, api
import string
from odoo.exceptions import ValidationError


class ExampleSon(models.Model):
    _name = 'example.son'
    _description = 'Hijo'
    _rec_name = 'name'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'sequence, id'

    """NO PERMITE REPETIR EL DOCUMENTO DE IDENTIDAD"""
    _sql_constraints = [
        ('identification_uniq', 'unique (identification)',
         'El documento de identidad debe ser único, por favor verifique e intente de nuevo'), ]

    """MANY2ONE"""
    employee_id = fields.Many2one('hr.employee', string='Familiar', required=True)
    identification_type_id = fields.Many2one('l10n_latam.identification.type', string='Tipo de identificacion',
                                             required=True, tracking=True, domain=[('id', 'in', [5, 9])])
    """CHAR"""
    name = fields.Char(string='Nombre', required=True)
    identification = fields.Char(string='Nro. de identificación', required=True)
    age = fields.Char(string='Edad', compute='_compute_age', store=True, readonly=False)
    """DATE"""
    date_birth = fields.Date(string='Fecha de nacimiento', required=True)
    """SELECTION"""
    scholarship = fields.Selection([('1', 'Ninguno'), ('2', 'Primaria'), ('3', 'Bachillerato'), ('4', 'Tecnico'),
                                    ('5', 'Tecnologo'), ('6', 'Profesional'), ('7', 'Especializacion'),
                                    ('8', 'Maestria'), ('9', 'Doctorado')], string='Escolaridad', required=True)
    """INTEGER"""
    sequence = fields.Integer(string='Secuencia')

    @api.model_create_multi
    def create(self, vals_list):
        res = super(ExampleSon, self).create(vals_list)
        for rec in res:
            rec.name = string.capwords(rec.name)
        return res

    @api.depends('date_birth', 'age')
    def _compute_age(self):
        for record in self:
            if record.date_birth:
                record.age = str(int((fields.Date.today() - record.date_birth).days / 365.25)) + ' años'
            else:
                record.age = False
