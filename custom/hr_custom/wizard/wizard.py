from odoo import models, fields, api, _
import string
from odoo.exceptions import ValidationError


class WizardCreateEmployee(models.TransientModel):
    _name = 'wizard.create.employee'
    _description = 'Ventana para crear el empleado'

    """MANY2ONE"""
    contact_id = fields.Many2one('res.partner', string='Contacto', domain="[('is_company', '=', False)]")

    def action_create_employee(self):
        contact = self.env['res.partner'].browse(self.contact_id.id)
        if self.contact_id:
            """VERIFICAR SI EXISTE UN EMPLEADO CON EL MISMO CONTACTO"""
            if self.contact_id.employees_count == 0:
                """CREA EL EMPLEADO"""
                employee = self.env['hr.employee'].create({
                    'name': contact.name,
                    'private_email': contact.email if contact.email else False,
                    'phone': contact.phone if contact.phone else False,
                    'address_home_id': contact.id if contact.id else False,
                    'address_home': contact.street if contact.street else False,
                    'identification_type_id': contact.l10n_latam_identification_type_id.id if contact.l10n_latam_identification_type_id.id else False,
                    'identification_id': contact.vat if contact.vat else False,
                })
                """LO REDIRIGE A LA VISTA DEL EMPLEADO CREADO"""
                return {
                    'name': _('Empleado'),
                    'view_mode': 'form',
                    'res_model': 'hr.employee',
                    'res_id': employee.id,
                    'type': 'ir.actions.act_window',
                    'target': 'current',
                }
            else:
                raise ValidationError(
                    _('El contacto %s ya tiene un empleado asociado, por favor verifique e intente nuevamente.') % (
                        contact.name,))
        else:
            return {
                'name': _('Nuevo'),
                'view_mode': 'form',
                'res_model': 'hr.employee',
                'type': 'ir.actions.act_window',
                'target': 'current',
            }
